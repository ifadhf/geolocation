# -*- coding: utf-8 -*-
{
    'name': "GeoLocation",

    'summary': """
        Custom Module to find location based on longitude and latitude""",

    'description': """
         Custom Module to find location based on longitude and latitude
    """,

    'author': "Fadhlullah",
    'website': "https://github.com/ifadhf",

    'category': 'Base',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/geolocal_view.xml',
        'views/doqar_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}