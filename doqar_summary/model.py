# -*- coding: utf-8 -*-

from datetime import datetime
from openerp import models, api
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as dt_format

class DoqarSummary(models.AbstractModel):
    _name = 'doqar.summary'
    _description = 'Doqar Summary'
        
    @api.model
    def _summary(self):
        # Compute obd_km and gps_km from doqar_campaign_history_detail 
        # returning string (value query to update summary data from doqar_campaign_history_detail)
        to_update_summary_value = self._update_detail_distance()
        
        # find update to table doqar_campaign_history_summary
        self._update_summary(to_update_summary_value)
    
    @api.model
    def _update_detail_distance(self):
        # Assumption : New records (that has the same key with processed records) has later date_time 
        # So we just calculate the km of new records, not all records
        # TZ is on UTC + 7 (to convert date_time to date)
        
        def hours_delta_str(a, b):
            seconds = (datetime.strptime(a, dt_format) - datetime.strptime(b, dt_format)).total_seconds()
            # if day before
            if seconds < 0:
                seconds += 86400.0  
            return seconds / 3600.0
        
        # Find all new history records
        query = """
            SELECT
                d1.id,
                d1.date_time + interval '7 hour' as date_time,
                d1.campaign_id,
                d1.road_type,
                d1.time_range,
                d1.day_type,
                d1.obd_speed, 
                d1.gps_speed,
                sub.last_time AS last_date_time
            FROM
                doqar_campaign_history_detail d1
                LEFT JOIN (
                    SELECT 
                        d2.campaign_id,
                        d2.road_type,
                        d2.time_range,
                        d2.day_type,
                        d2.date_time,
                        d2.date_time + interval '7 hour' AS last_time
                    FROM
                        doqar_campaign_history_detail d2
                    WHERE
                        d2.obd_km IS NOT NULL AND d2.gps_km IS NOT NULL
                    ORDER BY
                        date_time desc
                    LIMIT 1
                ) SUB ON (
                    SUB.campaign_id = d1.campaign_id AND
                    SUB.road_type = d1.road_type AND
                    SUB.time_range = d1.time_range AND
                    SUB.day_type = d1.day_type AND
                    to_char(SUB.date_time + interval '7 hour', 'YYYY-MM-DD') = to_char(d1.date_time + interval '7 hour', 'YYYY-MM-DD') AND
                    SUB.date_time < d1.date_time
                )
            WHERE
                d1.obd_km IS NULL AND d1.gps_km IS NULL
            ORDER BY 
                d1.campaign_id,
                d1.road_type,
                d1.time_range,
                d1.day_type,
                d1.date_time asc
            """
        self.env.cr.execute(query)
        result = self.env.cr.dictfetchall()
        
        # Compute obd_km and gps_km of history records
        last_key = None
        last_date_time = None
        to_update_summary_value = {}
        write_km_history_value = ''
        for rec in result:
            key = (rec['campaign_id'], rec['road_type'], rec['time_range'], rec['day_type'], rec['date_time'][:10])
            if key != last_key:
                last_key = key
                if rec['last_date_time']:
                    last_date_time = rec['last_date_time']
                else:
                    # First records of a key has 0 value of obd_km and gps_km
                    last_date_time = rec['date_time']
                
            obd_km = hours_delta_str(rec['date_time'], last_date_time) * rec['obd_speed']
            gps_km = hours_delta_str(rec['date_time'], last_date_time) * rec['gps_speed'] 
            if not to_update_summary_value.get(key):
                to_update_summary_value[key] = [0, 0]
            to_update_summary_value[key][0] += obd_km
            to_update_summary_value[key][1] += gps_km
            write_km_history_value += "(%s,%s,%s,%s,%s), " % (rec['id'],
                                                              self.env.user.id,
                                                              'now()',
                                                              obd_km,
                                                              gps_km)
            last_date_time = rec['date_time']
        
        # Update obd_km and gps_km of history records
        if write_km_history_value:
            query_write_detail = """
                UPDATE 
                    doqar_campaign_history_detail AS detail
                SET 
                    obd_km = d.obd_km,
                    gps_km = d.gps_km,
                    write_uid = d.write_uid,
                    write_date = d.write_date
                FROM (VALUES %s) as d(id, write_uid, write_date, obd_km, gps_km) 
                WHERE detail.id = d.id""" % write_km_history_value[:-2]
            self.env.cr.execute(query_write_detail)
        
        # Return key and new value of obd_km and gps_km
        return to_update_summary_value
    
    @api.model
    def _update_summary(self, to_update_summary_value):
        if not to_update_summary_value:
            return
        uid = self.env.user.id
        create_summary_value = ''
        for key in to_update_summary_value:
            create_summary_value += "(%s,'%s','%s','%s','%s', %s, %s, %s, %s, %s, %s), " % \
                                    (key + tuple(to_update_summary_value[key]) + (uid, uid, 'now()', 'now()'))
        
        if create_summary_value:
            query = """
                INSERT INTO doqar_campaign_history_summary AS d (%s) values %s 
                ON CONFLICT(%s) DO UPDATE SET 
                    total_obd_km = EXCLUDED.total_obd_km + d.total_obd_km,
                    total_gps_km = EXCLUDED.total_gps_km + d.total_gps_km,
                    write_uid = EXCLUDED.write_uid,
                    write_date = EXCLUDED.write_date
            """ % (','.join(self.summary_column), create_summary_value[:-2], ','.join(self.summary_key_column))
            self.env.cr.execute(query)

    summary_column = [
        'campaign_id',
        'road_type',
        'time_range',
        'day_type',
        'date',
        'total_obd_km',
        'total_gps_km',
        'create_uid',
        'write_uid',
        'create_date',
        'write_date'
    ]
    
    summary_key_column = [
        'campaign_id',
        'date',
        'road_type',
        'time_range',
        'day_type'
    ]
