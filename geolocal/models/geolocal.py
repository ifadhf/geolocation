# -*- coding: utf-8 -*-

# http://nssdc.gsfc.nasa.gov/planetary/factsheet/earthfact.html
# http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates
# https://developers.google.com/maps/documentation/geocoding/intro

import urllib, json, math, logging
from openerp import models, fields, api
from openerp.exceptions import UserError
import openerp.addons.decimal_precision as dp

key = 'AIzaSyAMHYQ2UJ-F_RGwqNpafsl-K-R8iAI_VHk'

_logger = logging.getLogger(__name__)    

class HistoryDetail(models.Model):
    _name = 'history.detail'
    _description = 'Location History'
    
    @api.multi
    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for history in self:
            result.append((history.id, '%s, %s' % (history.latitude, history.longitude)))
        return result
    
    latitude = fields.Float('Latitude', digits=dp.get_precision('Geo Location'))
    longitude = fields.Float('Longitude', digits=dp.get_precision('Geo Location'))
    address = fields.Char('Address')
    city = fields.Char('City')
    state = fields.Char('State')
    country = fields.Char('Country')
    
    @api.multi
    def find_address(self, context=None, batch=False):
        address = city = state = country = False
        
        query = """
            SELECT
                address,
                city,
                state,
                country
            FROM 
                geo_cache
            WHERE                
                round(latitude,3) = %0.3f AND 
                round(longitude,3) = %0.3f
            ORDER BY
                %f - latitude ASC,
                %f - longitude ASC
            LIMIT 1
        """ % (self.latitude, self.longitude, self.latitude, self.longitude,)
        self.env.cr.execute(query)
        res = self.env.cr.dictfetchall()
        if res:
            address = res[0]['address']
            city = res[0]['city']
            state = res[0]['state']
            country = res[0]['country']
        else:
            url = "https://maps.googleapis.com/maps/api/geocode/json?key=%s&latlng=%f,%f" \
                    % (key, self.latitude, self.longitude)
            data = json.load(urllib.urlopen(url))
            if data['status'] != 'OK':
                _logger.error('Error Code: %s' % data['status'])
                if not batch:
                    raise UserError ('Error Code: %s' % data['status'])
                return [False, False]
            address = data['results'][0]['formatted_address']
            for comp in data['results'][0]['address_components']:
                if 'administrative_area_level_2' in comp.get('types'):
                    city = comp.get('long_name')
                elif 'administrative_area_level_1' in comp.get('types'):
                    state = comp.get('long_name')
                elif 'country' in comp.get('types'):
                    country = comp.get('long_name')
                    
            # If we store the precise lat and long from google, query won't get the geo.cache if filtering 3 digits
            # maybe better store northwest+southeast?
            latitude = data['results'][0]['geometry']['location']['lat']
            longitude = data['results'][0]['geometry']['location']['lng']
            rad_lat = math.radians(latitude)
            rad_long = math.radians(longitude)
            create_cache_vals = {
                'latitude' : batch and "%s" % latitude or latitude,
                'longitude' : batch and "%s" % longitude or longitude,
                'rad_lat' : batch and "%s" % rad_lat or rad_lat,
                'rad_long' : batch and "%s" % rad_long or rad_long,
                'address' : batch and "'%s'" % address or address,
                'city' : batch and "'%s'" % city or city,
                'state' : batch and "'%s'" % state or state,
                'country' : batch and "'%s'" % country or country,
            }
            
        update_history_vals = {
            'address' : address,
            'city' : city,
            'state' : state,
            'country' : country,
            'id' : self.id
        }
        
        if batch:
            return [update_history_vals, not res and create_cache_vals]
        else:
            self.write(update_history_vals)
            if not res:
                self.env['geo.cache'].create(create_cache_vals)
    
    @api.model
    def _find_address(self, limit=None):
        write_history_value = ''
        create_cache_value = ''
        uid = self.env.user.id
        cache_column = [
                        'latitude',
                        'longitude',
                        'rad_lat',
                        'rad_long',
                        'address',
                        'city',
                        'state',
                        'country',
                        'create_uid',
                        'write_uid',
                        'create_date',
                        'write_date',
                        ]
        columns = ", ".join(cache_column)
        for history in self.search([('country', '=', False)], limit=limit):
            result = history.find_address(batch=True)
            if result[0]:
                write_history_value += "(%s, '%s', '%s', '%s', '%s', %s, %s), " % (
                                                            result[0]['id'],
                                                            result[0]['address'],
                                                            result[0]['city'],
                                                            result[0]['state'],
                                                            result[0]['country'],
                                                            uid,
                                                            'now()')
            if result[1]:
                result[1].update(create_uid='%s' % uid, write_uid='%s' % uid, create_date='now()', write_date='now()')
                create_cache_value += '(' + ', '.join([result[1][column] for column in cache_column]) + '), '
            
        query_write_history = """
            UPDATE 
                history_detail AS hd 
            SET 
                address = d.address,
                city = d.city,
                state = d.state,
                country = d.country,
                write_uid = d.write_uid,
                write_date = d.write_date
            FROM (VALUES %s) as d(id, address, city, state, country, write_uid, write_date) 
            WHERE hd.id = d.id""" % write_history_value[:-2]
        query_create_cache = "INSERT INTO geo_cache (%s) values %s" % (columns, create_cache_value[:-2])
        
        if write_history_value:
            self.env.cr.execute(query_write_history)
        if create_cache_value:
            self.env.cr.execute(query_create_cache)

class GeoCache(models.Model):
    _name = 'geo.cache'
    _description = 'Geolocation Cache'
    
    @api.multi
    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for cache in self:
            result.append((cache.id, '%s, %s' % (cache.latitude, cache.longitude)))
        return result
    
    latitude = fields.Float('Latitude', select=True, digits=dp.get_precision('Geo Location'))
    longitude = fields.Float('Longitude', select=True, digits=dp.get_precision('Geo Location'))
    rad_lat = fields.Float('Latitude (In Radian)', select=True, digits=dp.get_precision('Geo Location'))
    rad_long = fields.Float('Longitude (In Radian)', select=True, digits=dp.get_precision('Geo Location'))
    address = fields.Char('Address')
    city = fields.Char('City')
    state = fields.Char('State')
    country = fields.Char('Country')
    
        
    
