# -*- coding: utf-8 -*-
{
    'name': "Doqar Summary",

    'summary': """
        Custom Module to summary all travel distance
    """,
    'description': """
         Custom Module to summary all travel distance
    """,

    'author': "Fadhlullah",
    'website': "https://github.com/ifadhf",

    'category': 'Base',
    'version': '10.0',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'ir_cron_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}