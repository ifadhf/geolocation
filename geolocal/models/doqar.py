
from openerp import models, fields


class DoqarCampaignHistoryDetail(models.Model):
    _name = 'doqar.campaign.history.detail'
    _description = 'History Detail'
    
    date_time = fields.Datetime('Date Time')
    campaign_id = fields.Integer('Campaign')
    road_name = fields.Char('Road Name')
    road_type = fields.Char('Road Type')
    time_range = fields.Char('Time Range')
    day_type = fields.Char('Day Type')
    obd_km = fields.Float('OBD Km')
    gps_km = fields.Float('GPS Km')
    obd_speed = fields.Float('OBD Speed')
    gps_speed = fields.Float('GPS Speed')
    
class DoqarCampaignHistorySummary(models.Model):
    _name = 'doqar.campaign.history.summary'
    _description = 'Summary History'
    
    date = fields.Date('Date')
    campaign_id = fields.Integer('Campaign')
    road_type = fields.Char('Road Type')
    time_range = fields.Char('Time Range')
    day_type = fields.Char('Day Type')
    total_obd_km = fields.Float('OBD Km')
    total_gps_km = fields.Float('GPS Km')
    
    _sql_constraints = [
                     ('unique_key',
                      'unique(campaign_id,date,road_type,time_range,day_type)',
                      'Unique Constraint!')]
